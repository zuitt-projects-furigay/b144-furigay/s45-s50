// Mock Database
const  courseData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum keme keme keme 48 years cheapangga bonggakea buya ang borta jowabelles makyonget sangkatuts at na bakit nang majubis kasi na ang neuro shala boyband katol Cholo sa sa lorem ipsum keme keme pamenthol.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum keme keme keme 48 years cheapangga bonggakea buya ang borta jowabelles makyonget sangkatuts at na bakit nang majubis kasi na ang neuro shala boyband katol Cholo sa sa lorem ipsum keme keme pamenthol at nang.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum keme keme keme 48 years cheapangga bonggakea buya ang borta jowabelles makyonget sangkatuts at na bakit nang majubis kasi na ang neuro shala boyband.",
		price: 55000,
		onOffer: true
	},
	{
		id: "wdc004",
		name: "HTML - Introduction",
		description: "Lorem ipsum keme keme keme 48 years cheapangga bonggakea buya ang borta jowabelles makyonget sangkatuts at na bakit nang majubis kasi na ang neuro shala boyband.",
		price: 55000,
		onOffer: true
	}

];

export default courseData;