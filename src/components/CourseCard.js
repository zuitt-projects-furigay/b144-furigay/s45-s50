// import state hook from reacct 
//import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Button,Card } from 'react-bootstrap';
import { Link } from 'react-router-dom'

export default function CourseCard({courseProp}) {
	// Checks to see if the data was successfully passed
	// console.log(props);
	// console.log(typeof props);
	const {_id, name, description, price} = courseProp;

	// Use state hook in this component to be able to store its state
	//States are used to keep track of information related to individual components.

	/*
		Syntax: 

			const [getter, setter] = useState(initialGrtterValue);
	*/
	// const [count, setCount] = useState(0);
	// const [seats, setSeats] = useState(30);
	// const [isOpen, setIsOpen] = useState(false);

	// console.log(useState(0));

	//Function to keep tract of the enrolles for a course
	// function enroll() {
	// 	setCount(count + 1);
	// 	setSeats(seats - 1);

	// 	// if(seats == 0) {
	// 	// 	alert("No more seats available") 
	// 	// 	//document.getElementById("myBtn").disabled = true;

	// 	// }
		
	// 	// console.log('Enrollees: ' + count);
	// 	// console.log('Seats: ' - seats)
	// }

	// Define a useEffect hook to have the CourseCard component perform a certain task after every DOM update

	/*useEffect(() => {
		if(seats === 0){
			setIsOpen(true);
		}
	}, [seats])
	*/
	return (

		<Card className="mb-2">
		    <Card.Body>
		        <Card.Title>{name}</Card.Title>
		        <Card.Subtitle>Description:</Card.Subtitle>
		        <Card.Text>{description}</Card.Text>
		        <Card.Subtitle>Price:</Card.Subtitle>
		        <Card.Text>PhP {price}</Card.Text>		        
		        <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
		    </Card.Body>
		</Card> 
	)
}

/*
import { Card, Button } from 'react-bootstrap';

export default function CourseCard() {
    return (
       <Card>
           <Card.Body>
               <Card.Title>Sample Course</Card.Title>
               <Card.Subtitle>Description:</Card.Subtitle>
               <Card.Text>This is a sample course offering.</Card.Text>
               <Card.Subtitle>Price:</Card.Subtitle>
               <Card.Text>PhP 40,000</Card.Text>
               <Button variant="primary">Enroll</Button>
           </Card.Body>
       </Card> 
    )
}

*/

/*
<Row> 
	<Col>
		<Col xs={12} md={12}>
			<Card className="cardHighlight p-3">
			  <Card.Body>
			    <Card.Title>
			    	<h2>HTML 5</h2>
			    </Card.Title>
			    <Card.Title>
			    	<h5>Description:</h5>
			    </Card.Title>
			    <Card.Text>
			     	Basic HTML.
			    </Card.Text>
			    <Card.Title>
			    	<h5>Price:</h5>
			    </Card.Title>
			    <Card.Text>
			     	PhP 50,000
			    </Card.Text>
			    <Button variant="primary">Enroll</Button>
			  </Card.Body>
			</Card>
		</Col>
	</Col>
</Row>
*/